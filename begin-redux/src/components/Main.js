import React, { Component } from 'react';
import { Button, Grid } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { updateMenu } from '../store/actions';

class Main extends Component {
  handleMenuChange = (menuName) => {
    const { updateMenu } = this.props;
    updateMenu(menuName)
  }

  render() {
    const { activeMenu } = this.props;

    return (
      <div className="ui container"
        style={{textAlign: 'center', marginTop: 5 + 'rem'}}
      >
        <Grid columns={3}>
          <Grid.Row>
            <Grid.Column>
              <Button
                onClick={e => this.handleMenuChange('Blue')}
                color='blue'
                disabled={activeMenu === 'Blue' }
              >Blue</Button>
            </Grid.Column>
            <Grid.Column>
              <Button
                onClick={e => this.handleMenuChange('Violet')}
                color='violet'
                disabled={activeMenu === 'Violet' }
              >Violet</Button>
            </Grid.Column>
            <Grid.Column>
              <Button
                onClick={e => this.handleMenuChange('Purple')}
                color='purple'
                disabled={activeMenu === 'Purple' }
              >Purple</Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

// maps state of redux to props of this component
const mapStateToProps = state => {
  return {
    activeMenu: state.header.activeMenuName
  }
};

// for calling actions from props of our component
const mapDispatchToProps = dispatch => bindActionCreators(
  {
    updateMenu
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Main);
