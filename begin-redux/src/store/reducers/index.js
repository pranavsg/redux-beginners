export default (state = null, action) => {
  switch(action.type) {
    case 'UPDATE_MENU':
      return {
        ...state,
        activeMenuName: action.menuName  // or can use action.payload
      }
    default:
      return state;
  }
}
