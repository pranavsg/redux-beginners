import { createStore, combineReducers } from 'redux';

import HeaderSchema from './schema/Header.js';
import HeaderReducer from './reducers';

const schema = {
  header: HeaderSchema
};

const reducers = combineReducers({
  header: HeaderReducer
})

export const store = createStore(reducers, schema);

export default store;
