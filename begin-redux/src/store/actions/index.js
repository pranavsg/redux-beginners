export const updateMenu = menuName => {
  return {
    type: 'UPDATE_MENU',
    menuName // this is equal to menuName: menuName
  }
};
